<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title></title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <link rel="stylesheet" href="style.css">
  </head>
  <body class="contact <?php echo ($_GET['thank'] ? 'thankyou' : ''); ?>"  style="height: 880px;">
  <img src="header.jpg">
<?php if (!$_GET['thank']) : ?>
  <img src="contact-h1.png" class=h1 alt="contact us">
  <p>Thank you for your interest in Signal Tours. <br>Please fill the following form and choose a branch near you, and we will get back to you shortly.</p>
<form method=post action="send.php">
<label for="name">Full Name*</label>
<input required type="text" name=name>
<label for="email2">Email*</label>
<input required type="email" name=email2>
<input type="email" name=email>
<label for="subj">Subject*</label>
<input required type="text" name=subj>
<label for="branch">Branch Near You*</label>
<select name="branch"></select>
<label for="message">Message*</label>
<textarea required id="message" name="message" cols="30" rows="10"></textarea>
<button type="submit">SEND</button>
</form>
<?php else: ?>
<p>We received your request and will contact you by email shortly </p>
<?php endif; ?>
<div id="fb-root"></div>

<script src="https://connect.facebook.net/en_US/all.js"></script>
<script>
window.fbAsyncInit = function() { 
  FB.init({
    appId:  '1610837722515613',
    status: true,
    cookie: true,
    xfbml:  true
  });
  FB.Canvas.setAutoGrow(true);
  FB.Canvas.setSize(); 
}
function sizeChangeCallback() { FB.Canvas.setSize(); }
</script>
  </body>
<script>
var snifim = [
{add: ' (main branch)', bus: "Dan 24,28; Egged 572,531", phone:"03-5492436",mail:"sarab@signaltours.com",name:"Dalia Tserlin", address:"Sokolov St 33", city: "Ramat Hasharon", lat:32.1416178, lng:34.8366732},
{bus: "Metropoline 23, 62, 62א, 63, 63א, 70, 72,71", phone:"04-8200588",mail:"dorit.besman@signaltours.com",name:"Dorit Besman", address:"Bar Yehuda Road 300", city: "Haifa", lat:32.802524, lng:35.074807},
{bus: "Egged 14,4", phone:"09-8621957",mail:"malka@signaltours.com",name:"Malka Bronfman", address:"Smilanski St 10", city: "Netanya", lat:32.816674, lng:35.0006114},
{bus: "Dan 47,48; Egged 501,502,29,572", phone:"09-7410955",mail:"iris.hakim@signaltours.com",name:"Iris Hakim", address:"Ahuza St 109", city: "Ra'anana", lat:32.1802563, lng:34.8756248},
{bus: "Egged 9,32,17,19,22", phone:"02-5666567",mail:"osnatk@signaltours.com",name:"Osnat Kabiljo", address:"Aza St 14", city: "Jerusalem", lat:31.7736878, lng:35.216084},
{bus: "Egged 26,7; Afikim: 49,37; Metropoline 371,367", phone:"08-9481111",mail:"osnatk@signaltours.com",name:"Osnat Kabiljo", address:"Herzl St 182", city: "Rehovot", add:" - city center", lat:31.8967782, lng:34.8109002},
{bus: "Egged 205,274,201; Metropoline 371", phone:"08-9361303",mail:"rikyb@signaltours.com",name:"RickyBenl Benlolo", address:"HaMada Street 1", city: "Rehovot", add:" - Kiriat Hamada", lat:31.9120851, lng:34.803564},
{bus: "Egged 4", phone:"08-8556611",mail:"veredm@signaltours.com",name:"Vered Mitrany", address:"1 Habanim St.", city: "Ashdod", lat:31.795317, lng:34.639632},
{bus: "Dan 34,24,14,4", phone:"08-6218555",mail:"Jzilka@easygo.co.il",name:"Jacob Zilka", address:"Ha-Tikva St 12", city: "Be'er Sheva", lat:31.2476524, lng:34.7990831},
//{bus: "Egged 1,321; Metropoline 358,47", phone:"08-8562481",mail:"Jzilka@easygo.co.il",name:"Jacob Zilka", address:"Sderot Yigal Alon", city: "Dimona", lat:31.06344, lng:35.0360939},
]
$(document).ready(function(){
  $.each(snifim, function() {
    var snif = this.city + ' ' + (this.add || '');
    $('select').append($('<option>').val(snif + ' <' + this.mail + '>').text(snif));
  });
});
</script>
  </html>
